import Vapor
import AuthProvider
import FluentProvider
import Sessions

extension Droplet {
    func setupRoutes() throws {
        try setupUnauthenticatedRoutes()
    }

    /// Sets up all routes that can be accessed
    /// without any authentication. This includes
    /// creating a new User.
    private func setupUnauthenticatedRoutes() throws {
        post("register") { req in
            guard let json = req.json else {
                throw Abort(.badRequest)
            }
            guard let email = json["email"]?.string, let password = json["password"]?.string else {
                throw Abort(.badRequest)
            }
            
            let user = try User(json: json)
        
            guard try User.makeQuery().filter("email", user.email).first() == nil else {
                throw Abort(.badRequest, reason: "A user with that email already exists.")
            }
            user.password = try self.hash.make(password.makeBytes()).makeString()
            try user.save()
            let token = try Token.generate(for: user)
            try token.save()
            
            req.auth.authenticate(user)
            if (req.auth.isAuthenticated(User.self)) {
                print("User \(user.name) authenticated")
                print("User token: \(token.token)")
            } else {
                print("User not authenticated")
            }
            
//            let session = try req.assertSession()
//            try session.data.set("email", email)
            
            return user
        }
        
        get("gray") { req in
            return "Gray"
        }
        
        
        post("samplelogin") { req in
            guard let json = req.json else {
                throw Abort(.badRequest)
            }
            
            guard let email = json["email"]?.string, let password = json["password"]?.string else {
                throw Abort(.badRequest)
            }
            
            let passwordCredentials = Password(username:email, password:password)
            
            do {
                let user = try User.authenticate(passwordCredentials)
                req.auth.authenticate(user)
                if (req.auth.isAuthenticated(User.self)) {
                    print("User \(user.name) authenticated")
                } else {
                    print("User not authenticated")
                }
                return user
            }
        }
        
        
        get("userlist") { req in
//            guard let email = userListJson["email"]!.array else {
//                return "no users"
//            }
//            for name in email {
//                return name
//            }
            let userListJson = try User.all().makeJSON()
            guard userListJson["email"] != nil else {
                return "No registered users"
            }
            
            return userListJson
        }
        
        
        get("remember") { req in
            let session = try req.assertSession()
            
            guard let email = session.data["email"]?.string else {
                throw Abort(.badRequest, reason: "Please POST the name first.")
            }
            return email
        }
    }
}
